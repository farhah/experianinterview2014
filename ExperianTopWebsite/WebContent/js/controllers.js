var TopWebsites = angular.module('TopWebsites', []);

TopWebsites.controller('topwebController', function topwebController($scope, $http){
	$http.get('REST/WebService/JSON/').success( function(data){

		$scope.websites = data;
		//$scope.queryDate = new Date();

//		$scope.options = [{ label:'Website', value:'website'},
//		{label:'Visits', value: 'visits'}];
		//$scope.orderlySearch = $scope.options[1];

		$scope.limitNum = $scope.websites.length;
		$scope.changeLimit = function(limit){
			if($scope.checked){
				$scope.limitNum = limit;
			} 
			else{
				$scope.limitNum = $scope.websites.length;      
			}
		};

	});

	$scope.order = {};
	$scope.view = {};

	$scope.view.select = [{
		'name': 'Visit',
		'value': 'visits'
	},{
		'name': 'Website',
		'value': 'website'
	}];

	$scope.view.config = $scope.view.select[0].value;

	
//	$scope.order.configs = [{
//		'name': 'Ascending',
//		'value': ''
//			
//	},{
//		'name': 'Descending',
//		'value': 'reverse' 
//	}];
//
//	$scope.order.config = $scope.order.configs[1].value;


	$scope.topBottom = 'Bottom 5';
	$scope.orderChanged = function(){
		if($scope.descOrasc== 'reverse'){
			$scope.topBottom = 'Top 5';
		} 
		else{
			$scope.topBottom = 'Bottom 5';  
		}
	};

});

