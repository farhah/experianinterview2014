package com.farhah.experian;


import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

@Path("/WebService")
public class webservice {
	
	@GET
	@Path("/JSON")
	@Consumes(MediaType.APPLICATION_JSON)
	public String json()
	{
		String jsons  = null;
		try 
		{
			DBConnectionManager dbManager= new DBConnectionManager();
			ArrayList<DBObject> data = dbManager.GetObj();

			Gson gson = new Gson();
			jsons = gson.toJson(data);

		} catch (Exception e)
		{
			System.out.println(" error ---> " + e);
		}
		return jsons;
	}

}
