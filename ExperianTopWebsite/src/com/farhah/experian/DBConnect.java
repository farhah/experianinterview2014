package com.farhah.experian;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnect {
	
	public Connection connectToDB() throws Exception
	{
		try
		{
		String url = "jdbc:postgresql://localhost:5432/testdb";
		Class.forName("org.postgresql.Driver").newInstance();
		Connection connection = DriverManager.getConnection(url, "farhah", "");
	    return connection;
		}
		catch (SQLException e)
		{
		throw e;	
		}
		catch (Exception e)
		{
		throw e;	
		}
	}

}
