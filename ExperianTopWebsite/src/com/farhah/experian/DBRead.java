package com.farhah.experian;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class DBRead {

	public ArrayList<DBObject> GetDBObj(Connection conn) throws Exception
	{
		ArrayList<DBObject> dbObj = new ArrayList<DBObject>();
		try
		{
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM websitelog");
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				DBObject dbObject = new DBObject();
				
				String myDateStr = rs.getString("datadate"); 

				dbObject.setDataDate(myDateStr);
				dbObject.setWebsite(rs.getString("website"));
				dbObject.setVisits(rs.getInt("visits"));
				dbObj.add(dbObject);
			}
			rs.close();
			conn.close();
			return dbObj;
			
		}
		catch(Exception e)
		{
			System.out.println("error ----->" + e);
			throw e;
		}
	}
}
