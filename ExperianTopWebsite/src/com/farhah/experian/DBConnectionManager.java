package com.farhah.experian;

import java.sql.Connection;
import java.util.ArrayList;

public class DBConnectionManager {

	public ArrayList<DBObject> GetObj()throws Exception {
		ArrayList<DBObject> getObj = null;
		try {
			    DBConnect database= new DBConnect();
			    Connection con = database.connectToDB();
				DBRead read= new DBRead();
				getObj=read.GetDBObj(con);
		
		} catch (Exception e) {
			throw e;
		}
		return getObj;
	}

	
}
